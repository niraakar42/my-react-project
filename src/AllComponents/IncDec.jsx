import React, { useState } from "react";

const IncDec = () => {
  let [num, incNum] = useState(0);
  let handClick0 = () => {
    incNum(num + 1);
  };
  let [num1, decNum] = useState(0);
  let handClick1 = () => {
    decNum(num1 - 1);
  };
  return (
    <div>
      {num}
      <br></br>
      <button onClick={handClick0}>Increment</button>
      <br></br>
      {num1}
      <br></br>
      <button onClick={handClick1}>Decrement</button>
    </div>
  );
};

export default IncDec;
